
import os
import numpy as np


#data = np.genfromtxt('vf2.txt',usecols=(0,1,2),delimiter=',',dtype=None)
#myDic = {}
#for list in data:
#  myDic[list[0], list[1]] = list[2]


os.system('./occ_view_factor_calc --input=cube_test.obj --data_output=vf_matrix.txt')
data = np.genfromtxt('vf_matrix.txt',delimiter=',',dtype=None)
#occlusion creates many zero
assert( data[8:10, 74:76] = all zeros)
assert( data[6:8, 76:78] = all zeros)
assert( data[10:12, 72:74] = all zeros)
assert( data[6:8, 72:74] - data[10:12, 76:78] = all zeros)
assert( data[0:2, 66:68] - data[6:8, 72:74] = all zeros)
assert( data[0:2, 82:84] = data[4:6, 78:80] = all zeros) 
assert( data[4:6, 78:80] = data[6:8, 76:78] = all zeros)
#data = np.genfromtxt('cube_sparse.txt',usecols=(0,1,2),delimiter=',',dtype=None)
#myDic = {}
#for list in data:
#  myDic[list[0], list[1]] = list[2]
