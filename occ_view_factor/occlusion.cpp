

/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#include "occlusion.h"
#include "BulletCollision/CollisionShapes/btTriangleShape.h"
#include <boost/unordered_map.hpp>
#include <cmath>

btDoubleSidedTriangleRaycastCallback::btDoubleSidedTriangleRaycastCallback(const btVector3& from,const btVector3& to)
	:
	m_from(from),
	m_to(to),
	m_shouldDoBiggerSample(false),
	m_hitHappened(false)
{

}



void btDoubleSidedTriangleRaycastCallback::processTriangle(btVector3* triangle,int partId, int triangleIndex)
{
	const btVector3 &vert0=triangle[0];
	const btVector3 &vert1=triangle[1];
	const btVector3 &vert2=triangle[2];

	btVector3 v10; v10 = vert1 - vert0 ;
	btVector3 v20; v20 = vert2 - vert0 ;

	btVector3 triangleNormal; triangleNormal = v10.cross( v20 );
	
	//choose a point in the triangle's plane and see the coordinate of the point along the normal
	const btScalar dist = vert0.dot(triangleNormal);
	btScalar dist_a = triangleNormal.dot(m_from) ;
	dist_a-= dist;
	btScalar dist_b = triangleNormal.dot(m_to);
	dist_b -= dist;

	if ( dist_a * dist_b >= btScalar(0.0) )
	{
		return ; // same sign, it means that the m_from and m_to are on the same side of the triangle's plane
	}

	if (dist_a <= btScalar(0.0))
	{
		//std::cout << "btDoubleSidedTriangleRaycastCallback::processTriangle, back face overlap detected, but ignored! " << std::endl;
		// Backface, skip check
		//return;
	}

	
	const btScalar proj_length=dist_a-dist_b;
	const btScalar distance = (dist_a)/(proj_length);
	// Now we have the intersection point on the plane, we'll see if it's inside the triangle
	// Add an epsilon as a tolerance for the raycast,
	// in case the ray hits exacly on the edge of the triangle.
	// It must be scaled for the triangle size.
	
	btScalar triangle_square_area =triangleNormal.length2();		
	btScalar edge_tolerance = triangle_square_area * btScalar(-0.0001);
	//so, the algorithm now proceeds as follows: if the point is inside the triangle, if we take vectors from the point to the counter-clockwise vertices, each pair of cross products
	//will be aligned in the same direction. If the point lies outside the triangle, there will be a pair of crosses that will be reversed.
	btVector3 point; point.setInterpolate3( m_from, m_to, distance);
	{
		btVector3 v0p; v0p = vert0 - point;
		btVector3 v1p; v1p = vert1 - point;
		btVector3 cp0; cp0 = v0p.cross( v1p );

		if ( (btScalar)(cp0.dot(triangleNormal)) >=edge_tolerance) 
		{
					

			btVector3 v2p; v2p = vert2 -  point;
			btVector3 cp1;
			cp1 = v1p.cross( v2p);
			if ( (btScalar)(cp1.dot(triangleNormal)) >=edge_tolerance) 
			{
				btVector3 cp2;
				cp2 = v2p.cross(v0p);
				
				if ( (btScalar)(cp2.dot(triangleNormal)) >=edge_tolerance) 
				{
					m_hitHappened = true;
					//std::cout << "btDoubleSidedTriangleRaycastCallback::processTriangle, hit detected " << std::endl;
				  //@BP Mod
				  // Triangle normal isn't normalized
			      triangleNormal.normalize();

				 //@BP Mod - Allow for unflipped normal when raycasting against backfaces
					if (dist_a <= btScalar(0.0))
					{
						reportHit(-triangleNormal,distance,partId,triangleIndex);
					}
					else
					{
						reportHit(triangleNormal,distance,partId,triangleIndex);
					}
				}
			}
		}
		//some heuristics that we choose to do more tests if the triangle failed but they are close
		/*
		if (!m_hitHappened)
		{
			btVector3 center = btScalar(1./3.)*(vert0 + vert1 + vert2);
			btScalar distanceToCenter = btScalar(2.0)*(center-point).length2();
			if (distanceToCenter <= triangle_square_area)
				m_shouldDoBiggerSample = true;
		}*/
		
	}
}

void btDoubleSidedTriangleRaycastCallback::reportHit(const btVector3& hitNormalLocal, btScalar hitFraction, int partId, int triangleIndex )
{
	/*
	btCollisionWorld::LocalShapeInfo	shapeInfo;
	shapeInfo.m_shapePart = partId;
	shapeInfo.m_triangleIndex = triangleIndex;

	btVector3 hitNormalWorld = m_colObjWorldTransform.getBasis() * hitNormalLocal;

	btCollisionWorld::LocalRayResult rayResult
		(m_collisionObject,
		&shapeInfo,
		hitNormalWorld,
		hitFraction);

	bool	normalInWorldSpace = true;
	return m_resultCallback->addSingleResult(rayResult,normalInWorldSpace);
	*/
}


btTriangleMesh* Mesh::createBtMesh()
{
	btTriangleMesh* data = new btTriangleMesh();
	boost::unordered_map<  int ,  int > map;
	data->preallocateVertices(vertices.size());
	/*
	for (int i=0; i < vertices.size() ; i++)
	{
		map[i] = data->findOrAddVertex(vertices[i], false);
	}
	data->preallocateIndices(triangles.size());
	for (int i=0; i < triangles.size() ; i++)
	{
		Mesh::triangle& tri = triangles[i];
		data->addIndex(map[tri.v0]);
		data->addIndex(map[tri.v1]);
		data->addIndex(map[tri.v2]);
	}
	data->setNumOfTriangles(triangles.size());
	*/
	for (int i=0; i < triangles.size() ; i++)
	{
		Mesh::triangle& tri = triangles[i];
		data->addTriangle(vertices[tri.v0], vertices[tri.v1], vertices[tri.v2], false);
	}
	return data;
}


float ViewFactorCalculator::calculateViewFactor(unsigned int tA, unsigned int tB)
{
	Mesh::triangle& triA = meshObject->triangles[tA];
	Mesh::triangle& triB = meshObject->triangles[tB];
	btVector3 source, target;
	getRandomRayBetweenTriangles(tA, tB, source, target);
	btDoubleSidedTriangleRaycastCallback cb(source, target);
	bvhShape->performRaycast(&cb, source, target);
	int number_of_tests = 1;
	int number_of_hits = cb.hitDetected() ? 1 : 0;

	//if (cb.shouldDoABiggerSample())
	//{
		//std::cout << "ViewFactorCalculator::calculateViewFactor, bigger sample! " << std::endl;
		for (int test = 0; test < mc_samples; test++)
		{
			getRandomRayBetweenTriangles(tA, tB, source, target);
			btDoubleSidedTriangleRaycastCallback cbIn(source, target);
			bvhShape->performRaycast(&cbIn, source, target);
			number_of_tests++;
			if (cbIn.hitDetected())
				number_of_hits++;
		}
	//}

	//this is a simple MonteCarlo estimate of the amount of light let through between these two triangles
	float occlusion_factor = 1.0 - ( (float)number_of_hits / (float)number_of_tests );
	total_triangles_tested += number_of_tests;
	total_occlusions_detected += number_of_hits;
	/*
	if (cb.shouldDoABiggerSample())
		std::cout << " partial occlusion factor: " << number_of_hits << std::endl;*/
	
	if (occlusion_factor <0.001)
	{
		return 0.0;
	}
	btVector3 tA0, tA1, tA2;
	btVector3 tB0, tB1, tB2;
	tA0 = meshObject->vertices[triA.v0];
	tA1 = meshObject->vertices[triA.v1];
	tA2 = meshObject->vertices[triA.v2];
	tB0 = meshObject->vertices[triB.v0];
	tB1 = meshObject->vertices[triB.v1];
	tB2 = meshObject->vertices[triB.v2];
	btVector3 nA = (tA1 - tA0).cross(tA2 - tA0);
	btVector3 nB = (tB1 - tB0).cross(tB2 - tB0);
	btScalar A_a = btScalar(0.5)*(nA.length());
	btScalar B_a = btScalar(0.5)*(nB.length());
	nA.normalize();
	nB.normalize();
	btVector3 cA, cB;
	cA = btScalar(1./3.)*(tA0 + tA1 + tA2);
	cB = btScalar(1./3.)*(tB0 + tB1 + tB2);
	btScalar S2 = (cA-cB).length2();
	btVector3 nrAB = cB - cA;
	nrAB.normalize();
	// see http://en.wikipedia.org/wiki/View_factor#View_factors_of_differential_areas
	float view_factor = occlusion_factor * fabs( nA.dot(nrAB) ) * fabs( nB.dot(nrAB) ) * B_a / (PI * S2);
	//std::cout << " area A: " << A_a << " area B: " << B_a << " area squared distance: " << S2 << std::endl;
	//std::cout << " area A cosine: " << fabs( nA.dot(nrAB) ) << " area B cosine: " << fabs( nB.dot(nrAB) ) << std::endl;
	//if (view_factor > 0.01)
	//	std::cout << " view factor: " << view_factor << std::endl;
	return view_factor;
}

void ViewFactorCalculator::generateRandomTrianglePoint(float& a0, float& a1)
{
	boost::random::uniform_int_distribution<> dist(1, 10000);
	a0 = (float)dist(gen)/10000.0;
	a1 = (float)dist(gen)/10000.0;
  // if (a0+a1 > 1, reflect over a0+a1 = 1 axis
	if (a0+a1 > 1.0)
	{
		a0 = 1.0 - a0;
		a1 = 1.0 - a1; 
	}
		//std::cout << " random: u " << a0 << " v " << a1 << std::endl;
}

void ViewFactorCalculator::getRandomRayBetweenTriangles(unsigned int tA, unsigned int tB, btVector3& source, btVector3& target)
{
	Mesh::triangle& triA = meshObject->triangles[tA];
	Mesh::triangle& triB = meshObject->triangles[tB];
	btVector3 tA0, tA1, tA2;
	btVector3 tB0, tB1, tB2;
	tA0 = meshObject->vertices[triA.v0];
	tA1 = meshObject->vertices[triA.v1];
	tA2 = meshObject->vertices[triA.v2];
	tB0 = meshObject->vertices[triB.v0];
	tB1 = meshObject->vertices[triB.v1];
	tB2 = meshObject->vertices[triB.v2];
	// get 4 random points in [0, 1] interval, take each pair to describe a uniformly distributed random point inside each triangle

	float u, v;
	generateRandomTrianglePoint(u, v);
	source = tA0 + btScalar(u)*(tA1-tA0) + btScalar(v)*(tA2-tA0);

	generateRandomTrianglePoint(u, v);
	target = tB0 + btScalar(u)*(tB1-tB0) + btScalar(v)*(tB2-tB0);
}
