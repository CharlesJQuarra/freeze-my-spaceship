

#ifndef OCCLUSION_H
#define OCCLUSION_H

#include <btBulletDynamicsCommon.h>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <vector>

#define PI 3.141592653

struct Mesh
{
	std::vector< btVector3 > vertices;
	struct triangle
	{
		unsigned int v0, v1, v2;
	};
	std::vector< triangle > triangles;
	Mesh() : vertices(), triangles()
	{}

	btTriangleMesh* createBtMesh();
};

//the view factor with no occlusion between two surface elements is 
// F(i -> j ) = (n_i . r_ij) (n_j . r_ji) * A_j /(pi * r_ij^4)
// the occlusion heuristic is this:
// we test a ray from source to target. If there is candidate triangle, we check the distance
// between the ray intersection with the triangle's plane and the center of the triangle. If that distance is of the order of the square root
// of the triangle area (good criteria if the triangles are well-shaped) then we schedule a total of M ray tests
// at another random position in the target triangle. The occluded F_o(i->j) = F_n(i->j) * N_no / M
// where N_no is the number of ray tests out of M that gave no intersection against any triangle.
struct ViewFactorCalculator
{
	Mesh* meshObject;
	btTriangleMesh* btMesh;
	btBvhTriangleMeshShape* bvhShape;
	int total_occlusions_detected;
	int total_triangles_tested;
	boost::random::mt19937 gen;
	int mc_samples;

	ViewFactorCalculator(Mesh* _m, int _mc_s) : meshObject(_m), btMesh(meshObject->createBtMesh()), bvhShape(new btBvhTriangleMeshShape(btMesh,true,true)),
	total_occlusions_detected(0),
	total_triangles_tested(0), gen(),
	mc_samples(_mc_s) {}

	float calculateViewFactor(unsigned int tA, unsigned int tB);
	void getRandomRayBetweenTriangles(unsigned int tA, unsigned int tB, btVector3& source, btVector3& target);
	void generateRandomTrianglePoint(float& a0, float& a1);
};



class  btDoubleSidedTriangleRaycastCallback: public btTriangleCallback
{
	bool m_shouldDoBiggerSample;
	bool m_hitHappened;
public:

	//input
	btVector3 m_from;
	btVector3 m_to;


	btDoubleSidedTriangleRaycastCallback(const btVector3& from,const btVector3& to);
	
	virtual void processTriangle(btVector3* triangle, int partId, int triangleIndex);

	virtual void reportHit(const btVector3& hitNormalLocal, btScalar hitFraction, int partId, int triangleIndex );

	bool shouldDoABiggerSample()
	{
		return m_shouldDoBiggerSample;
	};

	bool hitDetected()
	{
		return m_hitHappened;
	};
	
};

#endif
