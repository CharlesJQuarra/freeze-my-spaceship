

#include <boost/program_options.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include "tiny_obj_loader.h"
#include "occlusion.h"

using namespace std;
namespace po = boost::program_options;

int main(int ac, char* av[])
{
	// Declare the supported options.
	po::options_description desc("Allowed options");
	desc.add_options()
		  ("help", "produce help message")
		  ("input", po::value<std::string>(), "input .obj geometry file")
			("data_output", po::value<std::string>(), "output txt file for the view factor calculation results")
			("mc_samples_per_pair", po::value<int>()->default_value(20), "number of MonteCarlo integration samples to compute in each triangle pair to estimate occlusion factor (default is 20)")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		  cout << desc << "\n";
		  return 1;
	}

	if (vm.count("input")) {
		  cout << "input file is: " 
	 << vm["input"].as<std::string>() << ".\n";
	} else {
		  cout << "Input file was not set.\n";
			return 1;
	}

	std::vector<tinyobj::shape_t> shapes;
	std::string err = tinyobj::LoadObj(shapes, vm["input"].as<std::string>().c_str(), 0);
	cout << "error: " << err << endl;
	
	Mesh mesh;
	for (size_t i = 0; i < shapes.size(); i++) {
			printf("shape[%ld].name = %s\n", i, shapes[i].name.c_str());
			for (size_t f = 0; f < shapes[i].mesh.indices.size(); f+= 3) {
				printf("  idx[%ld] = %d\n", f, shapes[i].mesh.indices[f]);
				Mesh::triangle tri;
				tri.v0 = shapes[i].mesh.indices[f];
				tri.v1 = shapes[i].mesh.indices[f+1];
				tri.v2 = shapes[i].mesh.indices[f+2];
				mesh.triangles.push_back(tri);
			}
			for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
				printf("  v[%ld] = (%f, %f, %f)\n", v,
					shapes[i].mesh.positions[3*v+0],
					shapes[i].mesh.positions[3*v+1],
					shapes[i].mesh.positions[3*v+2]);
					mesh.vertices.push_back(btVector3(shapes[i].mesh.positions[3*v+0], shapes[i].mesh.positions[3*v+1], shapes[i].mesh.positions[3*v+2]));
			}
	}

	ViewFactorCalculator vfc(& mesh, vm["mc_samples_per_pair"].as<int>());
	std::ofstream vf_out(vm["data_output"].as<std::string>(), std::ios::trunc);
	int total = 0 + pow( mesh.triangles.size() ,2 );
	int progress = 5;
	cout << " total nb of face pairs to check: " << total << endl;
	int tri_sizes = mesh.triangles.size();
	int eol_idx = tri_sizes-1;
	for (int fi = 0; fi < tri_sizes; fi++)
	{
		for (int fj = 0; fj < tri_sizes; fj++)
		{
			progress++;
			if (fi == fj)
			{
				
				vf_out << 0.0;
				if (fj < eol_idx)
					vf_out << ",";
				else
					vf_out << "\n";
				continue;
			}
			float viewFactor = vfc.calculateViewFactor(fi, fj);
			
			vf_out << viewFactor;

			if (fj < eol_idx)
				vf_out << ",";
			else
				vf_out << "\n";
			/*
			if (viewFactor > 1.0e-20)
				vf_out << fi << "," << fj << "," << viewFactor << "\n";*/
			/*
			float progress_rate = (float)progress / (float)total;
			int prog = rint( progress_rate * 100 );
			if ((viewFactor > 0.01) && (prog % 5 == 0))
			{
				cout << " progress rate: " << progress_rate * 100 << endl;
				cout << "view factor calculation between triangle " << fi << " and " << fj << endl;
				cout << " result: " << viewFactor << endl;
			}*/
		}
	}
	cout << " total triangle tests: " << vfc.total_triangles_tested << endl;
	cout << " hit rate: " << (float) vfc.total_occlusions_detected / (float) vfc.total_triangles_tested << endl;
	vf_out.close();
};
